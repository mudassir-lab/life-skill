# Good Practices for Software Development

## Question 1

1. Some companies use tools like Trello, Jira for documentation. But if your team is not using any such tool, you should write down the requirements and share it with the team. This will help you get immediate feedback. Also, this will serve as a reference for future conversations

2. Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.

3. Explain the problem clearly, mention the solutions you tried out to fix the problem

4. Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.

5. Remember they have their own work to do as well. Pick and choose your communication medium depending on the situation

6. Programming is essentially a result of your sustained and concentrated attention

## Question 2

1. I think I need to practice more to achive level of best coder.
2. For that I will try to do more projects in my free time.