# Tiny Habits

## Question 1

### Your takeaways from the video?

1. Do tiny changes in your life.
2. Set the habit you want to make right after something you already do.
3. When you know how to create tiny habits, you can change your life forever.
4. To start behaviour you should have 
    * Some amount of motivation
    * Ability
    * Trigger the behaviour
5. Celebrate small things in life.
6. Plant a tiny seed in right spot and it will grow without coaxing

## Question 2

### Your takeaways from the video?

1. Universal Formula for Human Behavior is B=MAP
    * M = Motivation
    * A = Ability
    * P = Prompt
2. Tiny Habits Methods involve 3 parts.

    1. Shrink: Hard tasks require more motivation so break down task in tiniest version so it will not require a lot motivation.
    2. Identify an Action Prompt: Set the habit you want to make right after something you already do.
    3. Shine: Celebrate tiny success as you celebrate big achievements.

## Question 3
### How can you use B = MAP to make making new habits easier?

![map](https://behaviormodel.org/wp-content/uploads/2020/08/Fogg-Behavior-Model-1024x683.jpg)

As we can see hard task require more motivation so we can break task in small task which will be easy to do and require less motivation.

## Question 4
### Why it is important to "Shine" or Celebrate after each successful completion of habit?

After celebrating each success our motivation will grow which will helps us to build difficult habits.

## Question 5
### Your takeaways from the video ?

1. If you improve yourself 1% daily you will be 37 times better at end of the year.
2. Good habits make time your ally.
Bad habits make time your enemy.
3.  Every time we perform a habit, we execute a four-step pattern: cue, craving, response, reward.
4. If we want to form new habits, we should make them obvious, attractive, easy, and satisfying.
5. You can use a habit tracker as a fun way to measure your progress and make sure you don’t fall off the wagon.
6. It is hard to change something if you are not aware of it. One way to become aware is a specific plan for when it going to happen.
7. You need to give your goals a time and place.


## Question 6
### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

There are three layers of behaviour change:
1. Identity
2. Process
3. Outcomes

Most of us work from outcome to identity rather than identity to outcome.

Make habits part of your identity. When solving a problem based on outcomes we only solve them temporarily.

To solve problem for long term we should change our identity.

For example: If someone wants quit smoking so rather than saying "I am trying quit smoke"
he should say "I am not smoker."

## Question 7
### Write about the book's perspective on how to make a good habit easier?

1. Make it obvious. Don’t hide your fruits in your fridge, put them on display front and centre.
2. Make it attractive. Start with the fruit you like the most, so you’ll actually want to eat one when you see it.
3. Make it easy. Don’t create needless friction by focusing on fruits that are hard to peel. Bananas and apples are super easy to eat, for example.
4. Make it satisfying. If you like the fruit you picked, you’ll love eating it and feel healthier as a result!

## Question 8
### Write about the book's perspective on making a bad habit more difficult?

1. Make it invisible.
2. Make it unattractive.
3. Make it difficult.
4. Make it unsatisfying.

## Question 9:
### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

One habit that I would like to do more of is exercising regularly. To make this habit more attractive and easier to follow, I can try to find an activity that I enjoy, such as dancing or cycling, so that I will be motivated to do it regularly.

To make the cue more obvious, I will associate the habit with a specific trigger, such as putting on my workout clothes as soon as I wake up in the morning.

To make the response more satisfying, I can reward myself after completing a workout, such as with a healthy snack or by taking a relaxing bath.

## Question 10:
### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

One habit that I would like to eliminate or do less of is procrastination. 

To make the cue invisible, I can try to avoid situations or environments that trigger my procrastination, such as by turning off notifications on my phone or finding a quiet place to work.

To make this habit less attractive and harder to follow, I can try to identify the underlying causes of my procrastination, such as fear of failure or lack of motivation, and work on addressing these issues.

To make the response unsatisfying, I can set consequences for myself if I procrastinate, such as by promising to donate money to a cause I don't support or by giving up a privilege or activity I enjoy. 




