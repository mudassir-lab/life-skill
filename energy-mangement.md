# Energy Management
## Question 1:
What are the activities you do that make you relax - Calm quadrant?
* Sleeping
* Watching light comedy shows
* Playing sports
* Praying
* Eating
* Exercise
 
## Question 2:
When do you find getting into the Stress quadrant?
* Stucked in work
* Sunny weather
* Missing deadlines
 
## Question 3:
How do you understand if you are in the Excitement quadrant?
* Over Happy
* Behaving like a child
* Something new out of routine
* After wining somthing
 
## Question 4
Paraphrase the Sleep is your Superpower video in detail.
* Sleep is a necessary thing.
* Not sleeping enough can cause many physical as well as mental illnesses.
* To learn new things sleep is required.
* Sleeping at the same time everyday and enough is a must.
* Cool weather helps to sleep better.
 
## Question 5
What are some ideas that you can implement to sleep better?
* Sleeping at the same time everyday and enough is a must.
* Room temperature is low.
* Exercising
* Remove stress causing elements.
* Avoid screens at night.
 
## Question 6
Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
* Increase attention and focus
* Avoids mental diseases
* Brings good mood.
* Develops creativity.
* Develops memory power.
 
## Question 7
What are some steps you can take to exercise more?
* Go to gym.
* Reduce use of motorcycles and cars for small distances.
* Walk daily after dinner.
* Take stairs.
* Play outdoor games.