# Grit and Growth Mindset

### Question 1

Paraphrase (summarize) the video in a few lines. Use your own words.

1. IQ is not the only difference between best and worst students.

2. Everyone learns if they work hard and long enough.

3. Success depends on grit, not on good looks, IQ, physical health, emotional intelligence.

4. Grit is passion and perseverance for very long-term goals. 

5. We should follow our commitments.

6. Grit is inversely related to talent

### Question 2

What are your key takeaways from the video to take action on?

1. We should follow our commitments.

2. Grit can come from growth mindset which is belief that the ability to learn is not fixed it can change with effort.

3. We need to measure whether we have been successful.

### Question 3

Paraphrase (summarize) the video in a few lines in your own words.

1. There are two types of people:
    1. Fixed Mindset:
    They believe that skills and intelligence are set and you either have the or you don't.
    2. Growth Midset:  
    They believe that skills and intelligence are grown and developed.

2. Growth Mindset is foundation for learning.

3. Fixed mindset people focus on performance and outcomes.

4. Growth mindset people
focus on getting better.

### Question 4

What are your key takeaways from the video to take action on?

1. We should look at efforts as a useful thing.

2. We should embrace challenges and work through them.

3. We should see mistakes as learning opportunities

4. We should appreciate feedback and use to improve ourselves.

### Question 5

What is the Internal Locus of Control? What is the key point in the video?

1. The Locus of Control is degree to which you believe you have control over your life.

2. We change our life by making efforts rather than blaming external factors.  

### Question 6

Paraphrase (summarize) the video in a few lines in your own words.

1. Belief that I can figure things out and I can get better enables us lifelong improvement.

2.  Question your assumptions as who am I?, Am I capable of that?

3. Develop your own life curriculum.

4. During failure time honour the struggle.

### Question 7
What are your key takeaways from the video to take action on?

1. Belief that I can figure things out and I can get better enables us lifelong improvement.

2.  Question your assumptions 

3. Develop your own life curriculum.

4. During failure time honour the struggle.

### Question 8

What are one or more points that you want to take action on from the manual? 

1. I will not leave my code unfinished till I complete the following checklist:
Make it work.
Make it readable.
Make it modular.
Make it efficient.

2. I will follow the steps required to solve problems:
Relax
Focus - What is the problem? What do I need to know to solve it?
Understand - Documentation, Google, Stack Overflow, Github Issues, Internet
Code
Repeat
3. I will understand the users very well. I will serve them and the society by writing rock solid excellent software.








