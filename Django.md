# Django Technical Paper

## 1.Setting files

### What is secret key? 

When you create a new Django project using startproject, the settings.py file is generated automatically and gets a random SECRET_KEY value. This value is the key to securing signed data – it is vital you keep this secure, or attackers could use it to generate their own signed values.

### What are default django apps?

y default, INSTALLED_APPS contains the following apps, all of which come with Django:

* **django.contrib.admin** – The admin site. You’ll use it shortly.

* **django.contrib.auth** – An authentication system.

* **django.contrib.contenttypes** – A framework for content types.

* **django.contrib.sessions** – A session framework.
django.contrib.messages – A messaging framework.

* **django.contrib.staticfiles** – A framework for managing static files.

These applications are included by default as a convenience for the common case.

Some of these applications make use of at least one database table, though, so we need to create the tables in the database before we can use them. 

### What is middleware? What are different kinds of middleware?

Middleware is a framework of hooks into Django’s request/response processing. It’s a light, low-level “plugin” system for globally altering Django’s input or output.

Each middleware component is responsible for doing some specific function. For example, Django includes a middleware component, AuthenticationMiddleware, that associates users with requests using sessions.

**Cache middleware**

class UpdateCacheMiddleware

class FetchFromCacheMiddleware

Enable the site-wide cache. If these are enabled, each Django-powered page will be cached for as long as the CACHE_MIDDLEWARE_SECONDS setting defines. 



    Getting Help 

    Language: en 

    Documentation version: 4.1 

Middleware¶

This document explains all middleware components that come with Django. For information on how to use them and how to write your own middleware, see the middleware usage guide.
Available middleware¶
Cache middleware¶

class UpdateCacheMiddleware¶

class FetchFromCacheMiddleware¶

Enable the site-wide cache. If these are enabled, each Django-powered page will be cached for as long as the CACHE_MIDDLEWARE_SECONDS setting defines. See the cache documentation.

**“Common” middleware**

class CommonMiddleware

Adds a few conveniences for perfectionists:

Forbids access to user agents in the DISALLOWED_USER_AGENTS setting, which should be a list of compiled regular expression objects.

Performs URL rewriting based on the APPEND_SLASH and PREPEND_WWW settings.

**Message middleware**

Quite commonly in web applications, you need to display a one-time notification message (also known as “flash message”) to the user after processing a form or some other types of user input.

For this, Django provides full support for cookie- and session-based messaging, for both anonymous and authenticated users. The messages framework allows you to temporarily store messages in one request and retrieve them for display in a subsequent request (usually the next one). Every message is tagged with a specific level that determines its priority (e.g., info, warning, or error).

**Session middleware**

Django provides full support for anonymous sessions. The session framework lets you store and retrieve arbitrary data on a per-site-visitor basis. It stores data on the server side and abstracts the sending and receiving of cookies. Cookies contain a session ID – not the data itself (unless you’re using the cookie based backend).

**CSRF protection middlewar**

class CsrfViewMiddleware

Adds protection against Cross Site Request Forgeries by adding hidden form fields to POST forms and checking requests for the correct value. 

###  Cross site scripting (XSS) protection

XSS attacks allow a user to inject client side scripts into the browsers of other users. This is usually achieved by storing the malicious scripts in the database where it will be retrieved and displayed to other users, or by getting users to click a link which will cause the attacker’s JavaScript to be executed by the user’s browser. However, XSS attacks can originate from any untrusted source of data, such as cookies or web services, whenever the data is not sufficiently sanitized before including in a page.

Using Django templates protects you against the majority of XSS attacks. However, it is important to understand what protections it provides and its limitations.

### Cross site request forgery (CSRF) protection

CSRF attacks allow a malicious user to execute actions using the credentials of another user without that user’s knowledge or consent.

Django has built-in protection against most types of CSRF attacks, providing you have enabled and used it where appropriate. However, as with any mitigation technique, there are limitations. For example, it is possible to disable the CSRF module globally or for particular views. You should only do this if you know what you are doing. There are other limitations if your site has subdomains that are outside of your control.

CSRF protection works by checking for a secret in each POST request. This ensures that a malicious user cannot “replay” a form POST to your website and have another logged in user unwittingly submit that form. The malicious user would have to know the secret, which is user specific (using a cookie).

### Clickjacking protection

Clickjacking is a type of attack where a malicious site wraps another site in a frame. This attack can result in an unsuspecting user being tricked into performing unintended actions on the target site.

Django contains clickjacking protection in the form of the X-Frame-Options middleware which in a supporting browser can prevent a site from being rendered inside a frame. It is possible to disable the protection on a per view basis or to configure the exact header value sent.

The middleware is strongly recommended for any site that does not need to have its pages wrapped in a frame by third party sites, or only needs to allow that for a small section of the site.

### WSGI

The Web Server Gateway Interface (WSGI, pronounced whiskey or WIZ-ghee) is a simple calling convention for web servers to forward requests to web applications or frameworks written in the Python programming language. The current version of WSGI, version 1.0.1, is specified in Python Enhancement Proposal (PEP) 3333.


The WSGI has two sides: 

* the server/gateway side. This is often running full web server software such as Apache or Nginx, or is a lightweight application server that can communicate with a webserver, such as flup.

* the application/framework side. This is a Python callable, supplied by the Python program or framework.

Between the server and the application, there may be one or more WSGI middleware components, which implement both sides of the API, typically in Python code.

WSGI does not specify how the Python interpreter should be started, nor how the application object should be loaded or configured, and different frameworks and webservers achieve this in different ways.

## 2. Models file

### What is ondelete Cascade?

Cascade emulates the SQL constraint of ON DELETE CASCADE. Whenever the referenced object (post) is deleted, the objects referencing it (comments) are deleted as well. This behaviour is a reasonable default and makes sense for most relationships — for this one as well. You don’t want orphaned comments lying around in your database when the associated post is deleted.

## Feild types

**AutoField**

class AutoField(**options)

An IntegerField that automatically increments according to available IDs. You usually won’t need to use this directly; a primary key field will automatically be added to your model if you don’t specify otherwise.

**BinaryField**

class BinaryField(max_length=None, **options)

A field to store raw binary data. It can be assigned bytes, bytearray, or memoryview.

By default, BinaryField sets editable to False, in which case it can’t be included in a ModelForm.

**BooleanField**

class BooleanField(**options)

A true/false field.

The default form widget for this field is CheckboxInput, or NullBooleanSelect if null=True.

The default value of BooleanField is None when Field.default isn’t defined.

**CharField**

class CharField(max_length=None, **options)

A string field, for small- to large-sized strings.

For large amounts of text, use TextField.

The default form widget for this field is a TextInput.

**DateField**

class DateField(auto_now=False, auto_now_add=False, **options)¶

A date, represented in Python by a datetime.date instance. 

**DateTimeField**

class DateTimeField(auto_now=False, auto_now_add=False, **options)

A date and time, represented in Python by a datetime.datetime instance.

**DecimalField**

class DecimalField(max_digits=None, decimal_places=None, **options)¶

A fixed-precision decimal number, represented in Python by a Decimal instance. It validates the input using DecimalValidator.

Has two required arguments:

DecimalField.max_digits:

The maximum number of digits allowed in the number. Note that this number must be greater than or equal to decimal_places.

DecimalField.decimal_places:

The number of decimal places to store with the number.

**EmailField**

class EmailField(max_length=254, **options)

A CharField that checks that the value is a valid email address using EmailValidator.

**IntegerField**

class IntegerField(**options)

An integer. Values from -2147483648 to 2147483647 are safe in all databases supported by Django.

It uses MinValueValidator and MaxValueValidator to validate the input based on the values that the default database supports.

The default form widget for this field is a NumberInput when localize is False or TextInput otherwise.

**JSONField**

class JSONField(encoder=None, decoder=None, **options)

A field for storing JSON encoded data. In Python the data is represented in its Python native format: dictionaries, lists, strings, numbers, booleans and None.

JSONField is supported on MariaDB 10.2.7+, MySQL 5.7.8+, Oracle, PostgreSQL, and SQLite (with the JSON1 extension enabled).

## Validators

A validator is a callable that takes a value and raises a ValidationError if it doesn’t meet some criteria. Validators can be useful for re-using validation logic between different types of fields.

For example, here’s a validator that only allows even numbers:

```python
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_even(value):
    if value % 2 != 0:
        raise ValidationError(
            _('%(value)s is not an even number'),
            params={'value': value},
        )
```
ou can add this to a model field via the field’s validators argument:
```python
from django.db import models

class MyModel(models.Model):
    even_field = models.IntegerField(validators=[validate_even])
```

**EmailValidator**

class EmailValidator(message=None, code=None, allowlist=None)

Parameters: 

* message – If not None, overrides message.

* code – If not None, overrides code.
        
* allowlist – If not None, overrides allowlist.

message:

The error message used by ValidationError if validation fails. Defaults to "Enter a valid email address".

code:

The error code used by ValidationError if validation fails. Defaults to "invalid".

**URLValidator**

class URLValidator(schemes=None, regex=None, message=None, code=None)

A RegexValidator subclass that ensures a value looks like a URL, and raises an error code of 'invalid' if it doesn’t.

Loopback addresses and reserved IP spaces are considered valid. Literal IPv6 addresses (RFC 3986#section-3.2.2) and Unicode domains are both supported.

In addition to the optional arguments of its parent RegexValidator class, URLValidator accepts an extra optional attribute:

schemes:

URL/URI scheme list to validate against. If not provided, the default list is ['http', 'https', 'ftp', 'ftps']. As a reference, the IANA website provides a full list of valid URI schemes.

**FileExtensionValidator**

class FileExtensionValidator(allowed_extensions, message, code)

Raises a ValidationError with a code of 'invalid_extension' if the extension of value.name (value is a File) isn’t found in allowed_extensions. The extension is compared case-insensitively with allowed_extensions.

### Difference between Python class and Python module

Classes in python are templates for creating objects. They contain variables and functions which define the class objects. At the same time, modules are python programs that can be imported into another python program. Importing a module enables the usage of the module’s functions and variables into another program.

## 3.Django ORM

### Using ORM queries in Django Shell

A QuerySet is, in essence, a list of objects of a given Model. QuerySets allow you to read the data from the database, filter it and order it.

Open up your local console (not on PythonAnywhere) and type this command:

```
(myvenv) ~/djangogirls$ python manage.py shell
```

The effect should be like this:

```
(InteractiveConsole)
>>>
```

You're now in Django's interactive console. It's just like the Python prompt, but with some additional Django magic. :) You can use all the Python commands here too.

Let's try to display all of our posts first. You can do that with the following command:

```
>>> from blog.models import Post
>>> Post.objects.all()
<QuerySet [<Post: my post title>, <Post: another post title>]>
```
his is how you create a new Post object in database:

```
>>> Post.objects.create(author=me, title='Sample title', text='Test')
```
But we have one missing ingredient here: me. We need to pass an instance of User model as an author. How do we do that?

Let's import User model first:
```
>>> from django.contrib.auth.models import User
```
What users do we have in our database? Try this:

```
>>> User.objects.all()
<QuerySet [<User: ola>]>
```
Let's get an instance of the user now 
```
>>> me = User.objects.get(username='ola')
```
As you can see, we now get a User with a username that equals 'ola'. Neat!

Now we can finally create our post:
```
>>>Post.objects.create(author=me, title='Sample title', text='Test')
<Post: Sample title>
```
Hurray! Wanna check if it worked?

```shell
>>> Post.objects.all()
<QuerySet [<Post: my post title>, <Post: another post title>, <Post: Sample title>]>
```
### Turning ORM to SQL in Django Shell

To convert ORM in SQL we can use following command:

```
>>> from ipl.models import Matches
>>> queryset = Matches.objects.all()
>>> print(queryset.query)
SELECT "ipl_matches"."id", "ipl_matches"."season", "ipl_matches"."city", "ipl_matches"."date", "ipl_matches"."team1", "ipl_matches"."team2", "ipl_matches"."toss_winner", "ipl_matches"."toss_decision", "ipl_matches"."result", "ipl_matches"."dl_applied", "ipl_matches"."winner", "ipl_matches"."win_by_runs", "ipl_matches"."win_by_wickets", "ipl_matches"."player_of_match", "ipl_matches"."venue", "ipl_matches"."umpire1", "ipl_matches"."umpire2", "ipl_matches"."umpire3" FROM "ipl_matches"
```

### What are Aggregations?

Grouping by queries is fairly common SQL operations, and sometimes becomes a source of confusion when it comes to ORM. 

**Basic GROUP BY and aggregations:**

Let’s start with basic count operations, which will return the dict containing the count of users:
```
>>> User.objects.aggregate(total_users=Count('id'))
```
Aggregate is used to the aggregate whole table.

### What are Annotations?

Most of the time we want to apply the aggregations to groups of rows, and for that, annotate can be used.

Let’s look at an example to group users based on is_staff:

```
>>> User.objects.values("is_staff").annotate(user_count=Count('*')
```

QuerySets and aggregations in Django

April 27, 2021 6 min read
Introduction

The object-relational mapper (ORM) in Django makes it easy for developers to be productive without prior working knowledge of databases and SQL. QuerySets represent a collection of objects from the database and can be constructed, filtered, sliced, or generally passed around without actually hitting the database. No database activity occurs until we do something to evaluate the QuerySet. In this guide, you will learn about how to perform these queries, both basic and advanced.

Throughout the guide, we will refer the django.contrib.auth.models.User model. You can insert multiple users into this model to test different QuerySets discussed in the following guide.

Moreover, we will be using the Django shell for running and testing the queries. You can start the Django shell with the following:

python manage.py shell

Basic Queries

Let’s start with some basic QuerySet operations.
Retrieving single objects

For cases in which you know there is only a single object that matches the query, you can use the get() method, which will return the object. Unlike filter, which always returns the QuerySet:

>>> user_id_1 = User.objects.get(id=1)

Note that if no results are found, it will raise a DoesNotExist exception, so better to use it in the try-except block:

try:
user_id_1 = User.objects.get(id=1)
except User.DoesNotExist:
print("User with id does not exists")

Getting an object from the QuerySet

There are two options for getting an object from the QuerySet.

The first is using first() and last(). First() returns the first object matched to the QuerySet, and last() returns the last object matched to the QuerySet:

from django.contrib.auth.models import User

>>> User.objects.filter(is_active=True).first()

>>> User.objects.filter(is_active=True).last()

The above query will return the first and last object matched with the Queryset.

The second option is latest() and earliest(). Latest() returns the latest object in the table based on the given fields, and earliest returns the earliest object in the table based on given fields:

from django.contrib.auth.models import User

>>> User.objects.latest('date_joined')

>>> User.objects.earliest('date_joined')

Field lookups

Field lookups deal with how you specify the SQL WHERE clause. Basic lookup keyword arguments take the form field__lookuptype=value. For example:

from datetime import datetime
## Get all users whose date_joined is less than today's date.
>>> User.objects.filter(date_joined__lte=datetime.today())

Searching the specific string (case sensitive):

## Get all user whose username string contains "user"
>>> User.objects.filter(username__contains = "user")

Or case insensitive:

## Get all user whose username string contains "user" (case insensitive)
>>> User.objects.filter(username__icontains = "user")

Or, starts-with and ends-with search:

## Get all user whose username string starts with "user"
>>> User.objects.filter(username__startswith = "user")
## Get all user whose username string ends with "user"
>>> User.objects.filter(username__endswith = "user")

You could also use case-insensitive versions called istartswith and iendswith.
Ordering QuerySets

After filtering the QuerySet, you can order it ascending or descending based on the given field(s).

The below query will first filter the users based on is_active, then by username in ascending order, and finally by date_joined in descending order. Note that - indicates the descending order of date_joined:

from django.contrib.auth.models import User

>>> User.objects.filter(is_active=True).order_by('username', '-date_joined')

Chaining filters

Django gives the option to add several filters to chain refinements together:

import datetime
from django.contrib.auth.models import User

>>> User.objects.filter(
... username__startswith='user'
... ).filter(
... date_joined__gte=datetime.date.today()
... ).exclude(
... is_active=False
... )

The above query initially takes all users, adds two filters, and excludes one. The final result is a QuerySet containing all users whose username starts with user, their date_joined being greater or equal to today’s date, and finally, excludes the inactive users.
Advanced queries

Now, that you understand the basic QuerySet operations, let’s now jump to advanced queries and QuerySet operations.
Set operations

Union() uses SQL UNION operator to combine the results of two or more QuerySets:

>>> qs1.union(qs2, qs3, ...)

Intersection() uses the SQL INTERSECTION operator to find common(shared) results of two or more QuerySets:

>>> qs1.intersection(qs2, qs3, ...)

Difference() uses the SQL EXCEPT operator to find elements present in the QuerySet but not in some other QuerySets:

>>> qs1.difference(qs2, qs3, ...)

Q objects

A Q() object represents an SQL condition that can be used in database-related operations. If you want to execute complex queries that contain OR, AND, and NOT statements, you can use Q() objects:

>>> from django.db.models import Q

>>> Q(username__startswith='user')
<Q: (AND: ('username__startswith', 'user'))>

For example, let’s find all users who are either staff or superusers:

>>> from django.contrib.auth.models import User

>>> User.objects.filter(Q(is_staff=True) | Q(is_superuser=True))

Similarly, you could use AND and NOT. In the below query, it finds all the users who are staff and whose usernames do not start with user:

>>> User.objects.filter(Q(is_staff=True) & ~Q(username__startswith='user'))

F objects

The F() object represents the value of a model field or annotated column. It makes it possible to refer to model field values and perform database operations using them without actually having to pull them out of the database into Python memory.
Over 200k developers use LogRocket to create better digital experiences
Learn more →

Let’s take an example of incrementing a hit count by one with the HitCount model of id=1.
Normally, one obvious way is to save it in memory, increment the count, and then save it:

site = HitCount.objects.get(id=1)
site.hits += 1
site.save()

The other way we can deal with this entirely by the database is by introducing the F() objects. When Django encounters an instance of F(), it overrides the standard Python operators to create an encapsulated SQL expression:

from django.db.models import F

site = HitCount.objects.get(id=1)
site.hits = F('hits') + 1
site.save()

F() offers performance advantages by:

    Getting the database, rather than Python, to perform operations
    Reducing the number of queries some operations require

Performing raw SQL queries

Django provides two ways of performing the raw SQL queries using raw() and connection.cursor().

For clarity, let’s take a basic query of fetching the non-staff users:

from django.contrib.auth.models import User

User.objects.filter(is_staff = False)

Executing raw queries

Raw() takes a raw SQL query, executes it, and returns a RawQuerySet instance, which can be iterated over like a normal QuerySet to provide object instances:

query = "select * from auth_user where is_staff=False;"
results = User.objects.raw(query)
for result in results:
print(result)

Executing the custom SQL directly

Sometimes even raw isn’t enough; you might need to perform queries that don’t map cleanly to models, or directly execute UPDATE, INSERT, or DELETE queries. In these cases, you can always access the database directly, routing around the model layer entirely.

For example, you can run the above SQL query using the cursor as demonstrated below:

from django.db import connection

query = "select * from auth_user where is_staff=False;"
with connection.cursor() as cursor:
cursor.execute(query)
print(cursor.fetchall())

Refer more on this topic from Django’s documentation here.
Getting raw SQL for a given QuerySet

To get the raw SQL query from a Django QuerySet, the .query attribute can be used. This will return the django.db.models.sql.query.Query object, which then can be converted to a string using __str__():

>>> queryset = MyModel.objects.all()
>>> queryset.query.__str__()
from django.contrib.auth.models import User

>>> queryset = User.objects.all()
>>> queryset.query
<django.db.models.sql.query.Query at 0x1ff0dcf7b08>

>>> queryset.query.__str__()
'SELECT "auth_user"."id", "auth_user"."password", "auth_user"."last_login", "auth_user"."is_superuser", "auth_user"."username", "auth_user"."first_name", "auth_user"."last_name", "auth_user"."email", "auth_user"."is_staff", "auth_user"."is_active", "auth_user"."date_joined" FROM "auth_user"'

Aggregation

Grouping by queries is fairly common SQL operations, and sometimes becomes a source of confusion when it comes to ORM. In this section, we will dive into applying GROUP BY and aggregations.
Basic GROUP BY and aggregations

Let’s start with basic count operations, which will return the dict containing the count of users:

>>> User.objects.aggregate(total_users=Count('id'))

Using annotate

Aggregate is used to the aggregate whole table. Most of the time we want to apply the aggregations to groups of rows, and for that, annotate can be used.

Let’s look at an example to group users based on is_staff:

```
>>> User.objects.values("is_staff").annotate(user_count=Count('*')
```

To perform group by in ORM style, we have to use the two methods values and annotate as follows:

* values(\<col>): Mention the fields for what to group by
* annotate(\<aggr function>): Mention what to aggregate using functions such as SUM, COUNT, MAX, MIN, and AVG

### What is a migration file? Why is it needed?

Migrations are Django’s way of propagating changes you make to your models (adding a field, deleting a model, etc.) into your database schema. They’re designed to be mostly automatic, but you’ll need to know when to make migrations, when to run them, and the common problems you might run into.

There are several commands which you will use to interact with migrations and Django’s handling of database schema:

* **migrate**, which is responsible for applying and unapplying migrations.
* **makemigrations**, which is responsible for creating new migrations based on the changes you have made to your models.
* **sqlmigrate**, which displays the SQL statements for a migration.
* **showmigrations**, which lists a project’s migrations and their status.

You should think of migrations as a version control system for your database schema. makemigrations is responsible for packaging up your model changes into individual migration files - analogous to commits - and migrate is responsible for applying those to your database.

### What are SQL Transaction?(Non ORM)

A SQL transaction is a grouping of one or more SQL statements that interact with a database. A transaction in its entirety can commit to a database as a single logical unit or rollback (become undone) as a single logical unit. In SQL, transactions are essential for maintaining database integrity. They are used to preserve integrity when multiple related operations are executed concurrently, or when multiple users interact with a database concurrently.

Typically, the beginning of a transaction in a SQL Server command line is defined using the BEGIN TRANSACTION statement:
```sql
BEGIN TRANSACTION NameOfTransaction;
```

If a database application determines that all of the changes for a transaction have succeeded, the application can use the COMMIT TRANSACTION statement. This commits those changes to the database, and it is placed at the end of a block of statements. In SQL Server, the following command is used to commit the transaction:

```sql
COMMIT TRANSACTION;
```

The example below obtains the largest CourseId value from table Course and adds 1 to it. It then inserts a row into the Course table and commits the transaction. Before committing, if any part of the CourseAdd transaction fails to execute, then none of the transactions can be processed. That means if the Select or Insert statement fails in some capacity, the entire transaction is null and void.
```sql
BEGIN TRANSACTION CourseAdd;

DECLARE @CrsId SMALLINT;

SELECT @CrsId = MAX(CourseId) + 1
FROM Course;

INSERT Course VALUES (@CrsId, 'Biology 101');

COMMIT TRANSACTION;
```

### Atomic Transaction

Atomicity is the defining property of database transactions. atomic allows us to create a block of code within which the atomicity on the database is guaranteed. If the block of code is successfully completed, the changes are committed to the database. If there is an exception, the changes are rolled back.

atomic blocks can be nested. In this case, when an inner block completes successfully, its effects can still be rolled back if an exception is raised in the outer block at a later point.

atomic is usable both as a decorator:
```python
from django.db import transaction

@transaction.atomic
def viewfunc(request):
    # This code executes inside a transaction.
    do_stuff()
```
and as a context manager:
```python
from django.db import transaction

def viewfunc(request):
    # This code executes in autocommit mode (Django's default).
    do_stuff()

    with transaction.atomic():
        # This code executes inside a transaction.
        do_more_stuff()
```




## References

1. https://www.pythonpool.com/python-class-vs-module/
2. https://docs.djangoproject.com/en/4.0/ref/validators/
3. https://docs.djangoproject.com/en/4.0/ref/models/fields/
4. https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface
5. https://docs.djangoproject.com/en/4.0/topics/security/
6. https://docs.djangoproject.com/en/4.1/topics/signing/
7. https://docs.djangoproject.com/en/4.1/ref/applications/
8. https://docs.djangoproject.com/en/4.1/topics/http/middleware/
9. https://docs.djangoproject.com/en/4.1/ref/middleware/
10. https://tutorial.djangogirls.org/en/django_orm/
11. https://blog.logrocket.com/querysets-and-aggregations-in-django/
12. https://www.linode.com/docs/guides/a-primer-on-sql-transactions/
