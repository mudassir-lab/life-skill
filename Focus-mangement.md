# Focus Management
## Question 1
What is Deep Work?
* Full attention
* Working with full focus
* Working without distractions
* Working for a significant amount of time
 
## Question 2
Paraphrase all the ideas in the above videos and this one in detail.
* Average durations of Deep Work: For beginners 1 hour is enough. In 1 hour, around 20 minutes time is taken to go into deep work. Hence, Cal says only approximately 40  to 45 minutes of work is achieved. Work in chunks. 
 
* Work should be completed way before the deadline. It is because to avoid any last minute blockers (like getting sick).
* A deadline can give bounds in order to plan our task.
* Deadlines have a negative aspect of putting pressure.
 
* Summary of Deep Work book:
   * Regular practice of Deep Work can make us achieve more deep work durations in a particular period of time.
   * Deep work helps us in completing great tasks.
   * Deep work is extremely valuable and extremely rare these days.
   * The work we complete in between deep works is termed as shallow work.
   * It is important to rest and end our day after having deep work sessions during the day.
 
## Question 3
How can you implement the principles in your day to day life?
* We can perform shallow work during our commutes.
* Team up with friends and schedule no talking session.
* If we get distracted during our work, notice the thought. Address the thought so that the distraction doesn't come to our mind again.
 
## Question 4
Your key takeaways from the video
   1. Life without social media does not make us disconnected from the world.
   1. Our jobs are not dependent on social media. 
   1. Social media is not harmless. It is addictive and can be compared to slot machines that are in our pockets the whole day.