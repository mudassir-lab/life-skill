# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

Any unwelcome verbal, visual, or physical conduct of a sexual nature that affects working conditions or creates a hostile work environment.

### Verbal Harassment

1. Comments about clothing
2. Comments about body
3. Sexual or gender based jokes or remarks
4. Requesting sexual favours or repeatedly asking a person out
5. Threats of demotion and asking for a sexual favour
6. Spreading rumours about a person's personal or sexual life

### Visual Harassment

1. Obscene posters
2. Drawing or pictures
3. Screensavers 
4. Cartoons
5. Email or text of sexual nature

### Physical Harassment

1. Sexual assault
2. Impeding or blocking movement
3. Inappropriate touching such as kissing, hugging, padding or rubbing.
4. Sexual gesturing, leering or staring

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

1. First of all tell the person to stop
2. If a person doesn't stop follow your organisation's policy
3. If don't know the policy ask supervisor or HR
4. Lodge a complaint
5. Make proof of an event 

