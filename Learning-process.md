# Learning Process

## Feynman Technique

### What is Feynman Technique?

The principle of Feynman tecnique is to understand the concept, you should be able to explain it to others in easy language.

Basically, try to explain things, who don't have the base knowledge you have.

### What are the different ways to implement this technique?

It has four steps.

1. Take a piece of paper and write the concept's name at the top.
2. Explain the concept using simple language.
3. Identify difficult areas, then go back to review the sources.
4. Make a list of difficult terms and try to make them simple.

## Learning how to learn

### Summary

* Basically our brain works in two mode
    1. Focus mode
    2. Relax mode
* While learning new concepts we should go back and forth in these modes.
* When we get stuck in a problem turn off focus mode and go take a rest for while to think about a problem in a new way.
* To avoid procastination use Pomodore tecnique.
* Pomodoro technique suggest that turn off all distraction for 25 minutes and work with focused attention. And then do something fun for a few minutes.
* Relaxing is also part of the learning process.
* To learn effectively
practice and exercise learning and remembering.
* Always test yourself and give yourself a mini-test.
* Try to recall things with closed eyes

### What are some of the steps that you can take to improve your learning process?

1. We can use the Pomodoro technique to learn new concepts
2. We can test ourselves time by time to check our knowledge
3. We should recall things with closed eyes.

## Learn anything in 20 hours

### Summary

* We can learn any skill in 20 hours
* Divide skill into smaller parts
* Learn enough to self correct
* Remove things which stops you to do practice
* Practice at least 20 hours with focus
* We can master any skill in 10,000 hours
* The major barrier to learn skill is not intellectual...
it's emotional.

### What are some of the steps that you can while approaching a new topic?

1. Divide skill into smaller parts
2. Learn enough to self correct
3. Remove things which stops you to do practice
4. Practice at least 20 hours with focused