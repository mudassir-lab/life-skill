## CSS Box Model

The components that can be depicted on the web page consist of one or more than one rectangular box.

A CSS box model is a compartment that includes numerous assets, such as edge, border, padding and material. It is used to develop the design and structure of a web page. It can be used as a set of tools to personalize the layout of different components. According to the CSS box model, the web browser supplies each element as a square prism.

The following diagram illustrates how the CSS properties of width, height, padding, border and margin dictate that how much space an attribute will occupy on a web page.

![CSS Box Model](https://static.javatpoint.com/csspages/images/css-box-model.png)

The CSS box model contains the different properties in CSS. These are listed below.

* Border
* Margin
* Padding
* Content

Now, we are going to determine the properties one by one in detail.

**Border Field**

It is a region between the padding-box and the margin. Its proportions are determined by the width and height of the boundary.

**Margin Field**

This segment consists of the area between the boundary and the edge of the border.

The proportion of the margin region is equal to the margin-box width and height. It is better to separate the product from its neighbor nodes.

**Padding Field**

This field requires the padding of the component. In essence, this area is the space around the subject area and inside the border-box. The height and the width of the padding box decide its proportions.

**Content Field**

Material such as text, photographs, or other digital media is included in this area.

It is constrained by the information edge, and its proportions are dictated by the width and height of the content enclosure.

**Elements of the width and height**

Typically, when you assign the width and height of an attribute using the CSS width and height assets, it means you just positioned the height and width of the subject areas of that component. The additional height and width of the unit box is based on a range of influences.

The specific area that an element box may occupy on a web page is measured as follows-

![size](box-model1.png)

## Inline vs Block Element

The inline and block elements of HTML are one of the important areas where web developers often get confused because they were unable to know which are inline and block elements which may cause clumsiness in a webpage in case he assumes some element to be a block but it is an inline element which causes next element comes next to it.

So let us see the differences between the inline and block elements in HTML and the different frequently used inline and block HTML elements. 

Block elements: They consume the entire width available irrespective of their sufficiency. They always start in a new line and have top and bottom margins. It does not contain any other elements next to it.

Examples of Block elements:

* \<h1>-\<h6> : This element is used for including headings of different sizes ranging from 1 to 6.

* \<div>: This is a container tag and is used to make separate divisions of content on the web page.

* \<hr>: This is an empty tag and is used for separating content by horizontal lines.

* \<li>: This tag is used for including list items of an ordered or unordered list.

* \<ul>: This tag is used to make an unordered list.

* \<ol>: This tag is used to make an ordered list.

* \<p>: This tag is used to include paragraphs of content in the webpage.

* \<table>: This tag is used for including the tables in the webpage when there is a need for tabular data.

HTML 5 Semantic block elements:

* \<header>: This tag is used for including all the main things of the webpage like navbar, logos, and heading of the webpage.

* \<nav>: This tag helps to navigate through different sections by including different blocks of hyperlinks in the webpage.

* \<footer>:  This contains all information about the authorization, contact, and copyright details of the webpage.

* \<main>: The main content of the webpage resides in this tag.

* \<section> : This is used separate different sections in the webpage.

* \<article>: This tag is used to include different independent articles on the webpage.

* \<aside>: This tag is used to mention details of the main content aside.

Example: 

```html
<!DOCTYPE html>
<html lang="en">
 
<head>
    <!--Meta data-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible"
          content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
 
    <!--The description written on title tag
        get appeared as browser tab name-->
    <title>Geeks For Geeks </title>
 
    <style>
        h1 {
            color: #006600;
            text-align: center;
            border: 2px solid #091418;
            background-color: yellow;
        }
        .container {
            border: 2px solid #040804;
            background-color: slategray;
        }
        p{
            border: 2px solid #040804;
            background-color:  #4089c5;
        }
    </style>
</head>
   
<!-- Whatever content in the body tag
     appears on the webpage -->
 
<body>
    <div class="container" >
        <h1>Geeks for Geeks(h1) </h1>
         
<p>
             This is a paragraph example of block
             element which occupied whole width (p)
        </p>
 
    </div>
</body>
 
</html>
```
**Output:** From the above output, 3 different block elements with different background colour and a border are used to show how the block elements occupy the whole width and the margin they leave. Three-block elements \<h1>,\<p>,\<div> are used in the above output.

![result](https://media.geeksforgeeks.org/wp-content/uploads/20210612001552/Screenshot20210612001513-660x212.png)

**Inline elements:** Inline elements occupy only enough width that is sufficient to it and allows other elements next to it which are inline. Inline elements don’t start from a new line and don’t have top and bottom margins as block elements have.

Examples of Inline elements:

* \<a>: This tag is used for including hyperlinks in the webpage.

* \<br>: This tag is used for mentioning line breaks in the webpage wherever needed.

* \<script> : This tag is used for including external and internal JavaScript codes.

* \<input>: This tag is used for taking input from the users and is mainly used in forms.

* \<img>: This tag is used for including different images in the webpage to add beauty to the webpage.

* \<span>:  This is an inline container that takes necessary space only.

* \<b>:  This tag is used in places where bold text is needed.

* \<label>: The tag in HTML is used to provide a usability improvement for mouse users i.e, if a user clicks on the text within the \<label> element, it toggles the control.

Example:

```html
<!DOCTYPE html>
<html lang="en">
 
<head>
    <!--Meta data-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content=
        "width=device-width, initial-scale=1.0">
 
    <style>
        h1 {
            color: #006600;
            text-align: center;
        }
 
        span {
            border: 2px solid #040804;
            background-color: #4089c5;
        }
 
        a {
            border: 2px solid #040804;
        }
    </style>
</head>
 
<!-- Whatever content in body tag
    appears on the webpage-->
<body>
    <div class="container">
        <h1>Geeks for Geeks</h1>
         
<p>
            This is a <span>span element </span>
            <span>and </span><b>this</b> is a
            <a href="#">link</a> which are examples
            of inline elements which occupy only
            sufficient width.
        </p>
 
    </div>
</body>
 
</html>
```
**Output:**

![output](https://media.geeksforgeeks.org/wp-content/uploads/20210612004027/Screenshot20210612004003-660x257.png)

From the above output, three different inline elements are used is \<span>, \<b>,\<a> that occupied only enough width and allowed other elements to settle at their next.

## Positioning:Relative/Absolute

**Take an example**

To start with, create a parent container with 4 boxes side by side.

![static](https://miro.medium.com/max/640/1*iVt-tUfGKHZyEkspuu7LlQ.webp)

index.html

```html
<div class=”parent”>
 <div class=”box” id=”one”>One</div>
 <div class=”box” id=”two”>Two</div>
 <div class=”box” id=”three”>Three</div>
 <div class=”box” id=”four”>Four</div>
</div>
```

style.css

```css
.parent {
 border: 2px black dotted;
 display: inline-block;
}.box {
 display: inline-block;
 background: red;
 width: 100px;
 height: 100px;
}#two {
  background: green;
}
```


By default, position is set to static. It positions based on the layout in the flow.

**What happens when we want to move the GreenBox but do not want to affect the layout around it?**

![relative](https://miro.medium.com/max/640/1*3N2ousp3yth9ovHA8TpDZw.webp)

This is where position relative comes in. Move the green box relative to its current position to 20px from the left and top without changing the layout around it. Thus, leaving a gap for the green box where it would have been had it not been position.

```css
#two {
  top: 20px;
  left: 20px;
  background: green;
  position: relative;
}
```


**Position: absolute is the opposite.**

![absolute](https://miro.medium.com/max/640/1*XDqGjAEa_sNL1OlPQhEb7A.webp)

By applying position: absolute to the GreenBox, it will not leave any gap where it would have been. The position of the GreenBox is based on its parent position (the dotted border). Thus, moving 20px to the left and bottom from the top-left origin of the dotted border.

```css
#two {
  top: 20px;
  left: 20px;
  background: green;
  position: absolute;
}
```


**In a nutshell**


**position: relative** places an element relative to its current position without changing the layout around it, whereas **position: absolute** places an element relative to its parent’s position and changing the layout around it.

![psition](https://miro.medium.com/max/640/1*pe9E2kzrX48Wwn_0wKklmw.webp)

## Common CSS structural classes

Structural pseudo classes allow access to the child elements present within the hierarchy of parent elements. We can select first-child element, last-child element, alternate elements present within the hierarchy of parent elements.

The following is the list of structural classes.

**1. :first-child**

:first-child represents the element that is prior to its siblings in a tree structure.

Example

```css
<style>
table tr:first-child{
 background-color:gray;
}
</style>
```

**2. :nth-child(n)**

:nth-child(Expression)
class applies CSS properties to those elements that appear at the position evaluated by the resultant of an expression. The expression evaluates to a value resulting in the position of the element in a tree structure.

For example, :nth-child(2n+1) pseudo class applies to the rows of table that appear at the position of the given expression.

tr:nth-child(2n+1) represents rows such as 1th, 3th, 5th, 7th.... for the n values of 0, 1, 2, 3.......

Example
```css
<style>
table tr:nth-child(2n+1){
 background-color:gray;
}
</style>
```
It means the background-color of the 1th, 3th, 5th , etc, element is gray.

**3. :last-child**

:last-child pseudo class represents the element that is at the end of its siblings in a tree structure.
Example

```css
<style>
ul li:last-child{
 background-color:lightblue;
}
</style>
```

It means the background-color of the last child of unordered list is lightblue.

**4. :nth-last-child(n)**

:nth-last-child(Expression) is the same as :nth-child(Expression) but the positioning of elements start from the end.

tr:nth-last-child(2n+1) represents last row, 3th last row, 5th last row, etc, element.

Example

```css
<style>
table tr:nth-last-child(2n+1){
 background-color:lightblue;
}
</style>
```

:nth-last-child(1) and :nth-last-child(2) means the last, 2nd last elements in the list of siblings.

**5. :only-child**

:only-child represents the element that is a sole child of the parent element and there is no other sibling.

Example

```css
<style>
div p:only-child{
 background-color:lightblue;
}
</style>
```

It means the first and last elements are same.

**6. :first-of-type**

There might be more than one type of siblings under a common parent. It selects the first element of the one type of sibling.

Example

```css
<style>
dl dd:first-of-type{
 background-color:lightblue;
}
</style>
```

In the above example, there are two types of siblings i.e. dd and dt. And we choose the \<dl> element to apply the CSS properties. It is the same as :nth-child(1).

**7. :nth-of-type(n)**

There may be more than one elements of the same type under a common parent. :nth-of-type(Expression) represents those elements of the same type at the position evaluated by the Expression.

Example
```css
<style>
tr td:nth-of-type(2n+1){
 background-color:gray;
}
tr td:nth-of-type(2n){
 background-color:lightblue;
}
</style>
```

**8. :last-of-type**

:last-of-type represents the last element in the list of same type of siblings.

Example

```css
<style>
dl dd:last-of-type{
 background-color:lightblue;
}
</style>
```

**9. :nth-last-of-type(n)**

It is the same as :nth-of-type(n) but it starts counting of position from the end instead of start.

Example

```css
<style>
body > h2:nth-last-of-type(n+6){
 color:blue;
}
</style>
```
## Common CSS Styling Classes

A CSS selector selects the HTML element(s) for styling purpose. CSS selectors select HTML elements according to its id, class, type, attribute etc.

There are many basic different types of selectors.

* Element Selector
* Id Selector
* Class Selector
* Universal Selector
* Group Selector
* Attribute Selector
* Pseudo-Class Selector
* Pseudo-Element Selector

**1. Element selector:** The element selector selects HTML elements based on the element name (or tag) for example p, h1, div, span, etc.

**2. Id selector:** The id selector uses the id attribute of an HTML element to select a specific element.

**3. Class-selector:** The class selector selects HTML elements with a specific class attribute.

**4. Universal-selector:** The Universal selector (*) in CSS is used to select all the elements in a HTML document. It also includes other elements which are inside under another element.

**5. Group-selector:** This selector is used to style all comma separated elements with the same style.

**6. Attribute Selector :** The attribute selector [attribute] is used to select the elements with a specified attribute or attribute value.

**7.Pseudo-Class Selector**: It is used to style a special type of state of any element. For example- It is used to style an element when a mouse cursor hovers over it.

**8.Pseudo-Element Selector :** It is used to style any specific part of the element. For Example- It is used to style the first letter or the first line of any element.

## CSS Specificity

Let’s specifically cover this subject.

The best way to explain it is to start with an example of where specificity gets confusing and perhaps doesn’t behave like you would expect. Then we’ll take a closer look at how to calculate the actual specificity value to determine which selector takes precedence.

Here is a simple unordered list:

```html
<ul id="summer-drinks">
   <li>Whiskey and Ginger Ale</li>
   <li>Wheat Beer</li>
   <li>Mint Julip</li>
</ul>
```

Now you want to designate one of these your favorite drink and change its styling a bit. You need a hook for this so you apply it via a class name on the list element.

```html
<ul id="summer-drinks">
   <li class="favorite">Whiskey and Ginger Ale</li>
   <li>Wheat Beer</li>
   <li>Mint Julip</li>
</ul>
```

Now you pop open your CSS and do your styling for your new class:

```css
.favorite {
  color: red;
  font-weight: bold;
}
```

Then you take a look at your work, but alas, it didn’t work! The text of your favorite drink didn’t turn red or go bold! Something fishy is at work here.

Poking around more in the CSS, you find this:

```css
ul#summer-drinks li {
   font-weight: normal;
   font-size: 12px;
   color: black;
}
```

There is your trouble right there. Two different CSS selectors are telling that text what color and font-weight to be. There is only one statement for font-size, so clearly that one will take effect. These aren’t “conflicts” per-say, but the browser does need to decide which one of these statements to honor. It does so by following a standard set of specificity rules.

I think this confuses some beginners because they haven’t quite gotten this sorted out yet. They might think because the .favorite statement is “further down in the CSS” or because the class=”favorite” is “closer to the actual text” in the HTML that will be the one that “wins”.

In fact, the order of selectors in your CSS does play a role and the “further down” one does in fact win when the specificity values are exactly the same. For example:

```css
.favorite {
   color: red;
}
.favorite {
   color: black;
}
```

The color will be black… but I digress.

The point here is you want to be as specific as it makes sense to be every chance you get. Even with the simple example presented above, it will become obvious to you eventually that simply using a class name to target that “favorite drink” isn’t going to cut it, or won’t be very safe even if it did work. It would have been smart to use this:

```css
ul#summer-drinks li.favorite {
  color: red;
  font-weight: bold;
}
```

That is what I’m calling “being as specific as it makes sense to be”. You could actually be way more specific and use something like this:

```css
html body div#pagewrap ul#summer-drinks li.favorite {
  color: red;
  font-weight: bold;
}
```

But that is over the top. It makes your CSS harder to read and yields no real benefits. Another way to juice up the specificity value for your “.favorite” class is to use the !important declaration.

```css
.favorite {
  color: red !important;
  font-weight: bold !important;
}
```
I once heard it said that !important is like the Jedi mind trick for CSS. Indeed it is, and you can force your will over the styling of elements by using it. But !important imposes that will through drastically increasing the specificity of that particular selectors property.

The !important declaration can be easily misused if misunderstood. It is best used to keep your CSS cleaner, in examples where you know elements with a particular class selector should use a certain set of styling no matter what. Conversely, not used just as a quick crutch to override the styling of something instead of figuring out how the CSS was structured and working by the original author.

One of my classic examples is:

```css
.last {
   margin-right: 0 !important;
}
```

I often use that in situations where there are multiple floated blocks, for the last block on the right in a row. That ensures the last block doesn’t have any right margin which would prevent it from butting snugly against the right edge of its parent. Each of those blocks probably has more specific CSS selectors that apply the right margin to begin with, but !important will break through that and take care of it with one simple/clean class.

### Calculating CSS Specificity Value

Why is that our first attempt at changing the color and font-weight failed? As we learned, it was because simply using the class name by itself had a lower specificity value and was trumped by the other selector which targeted the unordered list with the ID value. The important words in that sentence were class and ID. CSS applies vastly different specificity weights to classes and IDs. In fact, an ID has infinitely more specificity value! That is, no amount of classes alone can outweigh an ID.

Let’s take a look at how the numbers are actually calculated:

![img1](https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/01/specificity-calculationbase_rhrovi.png?w=570&ssl=1)

In otherwords:

If the element has inline styling, that automatically1 wins (1,0,0,0 points)
For each ID value, apply 0,1,0,0 points
For each class value (or pseudo-class or attribute selector), apply 0,0,1,0 points
For each element reference, apply 0,0,0,1 point
You can generally read the values as if they were just a number, like 1,0,0,0 is “1000”, and so clearly wins over a specificity of 0,1,0,0 or “100”. The commas are there to remind us that this isn’t really a “base 10” system, in that you could technically have a specificity value of like 0,1,13,4 – and that “13” doesn’t spill over like a base 10 system would.

### Sample calculations

![img2](https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/01/cssspecificity-calc-1_kqzhog.png?w=570&ssl=1)

![img3](https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/01/cssspecificity-calc-2.png?w=570&ssl=1)

![img4](https://i0.wp.com/css-tricks.com/wp-content/uploads/2010/05/cssspecificity-calc-3v2.jpg?w=570&ssl=1)

Update: The :not() sort-of-pseudo-class adds no specificity by itself, only what’s inside the parens is added to specificity value.

![img5](https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/01/cssspecificity-calc-4.png?w=570&ssl=1)

![img6](https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/01/cssspecificity-calc-5-1.png?w=570&ssl=1)

### Important Notes

* The universal selector (*) has no specificity value (0,0,0,0)

* Pseudo-elements (e.g. :first-line) get 0,0,0,1 unlike their psuedo-class brethren which get 0,0,1,0

* The pseudo-class :not() adds no specificity by itself, only what’s inside it’s parentheses.

* The !important value appended a CSS property value is an automatic win. It overrides even inline styles from the markup. The only way an !important value can be overridden is with another !important rule declared later in the CSS and with equal or great specificity value otherwise. You could think of it as adding 1,0,0,0,0 to the specificity value.

## CSS Responsive Queries

Media types first appeared in HTML4 and CSS2.1, which enabled the placement of separate CSS for screen and print. In this way, it was possible to set separate styles for a page’s computer display vis-à-vis its printout.

```html
<link rel="stylesheet" type="text/css" href="screen.css" media="screen">
<link rel="stylesheet" type="text/css" href="print.css"  media="print">
```
or

```css
@media screen {
    * {
        background: silver
    }
}
```

In CSS3, you can define styles depending on the page width. As page width correlates with the size of the user’s device, this capability thus allows you to define different layouts for different devices. Note: media queries are supported by all major browsers.

This definition is possible through the setting of basic properties: max-width, device-width, orientation, and color. Other definitions are possible as well; but in this, case the most important things to note are minimum resolution (width) and orientation settings (landscape vs. portrait).

The responsive CSS example below shows the procedure for initiating a certain CSS file with respect to the page width. For example, if 480px is the maximum resolution of the current device’s screen, then the styles defined in main_1.css will be applied.

```html
<link rel="stylesheet" media="screen and (max-device-width: 480px)" href="main_1.css" />
```

We can also define different styles within the same CSS stylesheet such that they are only utilized if certain constraints are satisfied. For example, this portion of our responsive CSS would only be used if the current device had a width above 480px:

```css
@media screen and (min-width: 480px) {
    div {
        float: left;
        background: red;
    }
    .......
}
```
## Flexbox

The Flexbox model allows us to layout the content of our website. Not only that, it helps us create the structures needed for creating responsive websites for multiple devices. 

### Flexbox Architecture

So how does Flexbox architecture work? The flex-items [Contents] are distributed along the main axis and cross axis. And, depending on the flex-direction property, the layout position changes between rows and columns.

![architecture](https://dev-to-uploads.s3.amazonaws.com/i/hy2oqjvsbk60ef92nktg.png)

### Flexbox Chart

This chart contains every possible property and value you can use when you're working with Flexbox. You can reference it while doing your projects and experiment with different values.

![chart](https://dev-to-uploads.s3.amazonaws.com/i/gv3jyh4xt4fbwtq1qejn.png)

## Grid

Grid is a blueprint for making websites.

The Grid model allows you to layout the content of your website. Not only that, it helps you create the structures you need for building responsive websites for multiple devices. This means your site will look good on desktop, mobile, and tablet.


### CSS Grid Architecture

So how does Grid architecture work? The Grid items [Contents] are distributed along the main axis and cross axis. Using various Grid properties, you can manipulate the items to create your website layouts.

![arch](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/h9qs07pm0a8s20scr6wr.png)

By the way, you can join multiple rows and columns, just like in Excel software, which gives you more flexibility and options than Flexbox.

### CSS Grid Chart

This chart contains every possible property you can use when using grid. Grid properties are divided into:

    Parent properties
    Child Properties

Note: The red colored text denotes the shorthand properties:

![chart1](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/n93mkan7du7wz3zyibtw.png)

![chart2](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/5g3msf9v3yw9qjpzkvj7.png)

## Meta Tags

HTML lets you specify metadata - additional important information about a document in a variety of ways. The META elements can be used to include name/value pairs describing properties of the HTML document, such as author, expiry date, a list of keywords, document author etc.

### Title

Strictly speaking, the title is an independent HTML tag rather than a meta element, but due to its interaction with user agents, it is often associated with metadata. As a mandatory element in the HTML head, the title is generally placed before any further meta tags. Read by web browsers, the title appears as a default name when setting a bookmark, in the title bar of a tab or window and in the web browser’s search history. Additionally, the title tag is used by all the biggest search engines as a header on the search engine results pages (SERPs). It therefore has a decisive influence on the clicking behavior of search engine users. 

```html
<title>the most important meta tags and their functions | IONOS</title>
```
### Character encoding

If the font is not already defined in the HTTP header, this information needs to be given in the HTML code. This will ensure, amongst other things, that any special characters are displayed correctly. Use the following meta-tag to specify the font or character coding:

```html
<meta charset="utf-8"/>
```
### Description

The description, also known as the meta description, offers room for a brief summary of the page content. Since search engines present this meta tag as a snippet in the SERPs, careful editing is absolutely imperative for any content that’s displayed here. Due to its significant influence on web users’ click behavior, the description is one of the most important HTML meta tags for search engine optimization. However, it is important for website owners to remember to limit the description to 160 characters (including spaces). Any text after this point is cut off and an incomplete description will appear in the search results. 

```html
<meta name="description" content="HTML meta tags are a cornerstone of coding. But which are the most essential? We give you a rundown of all the meta tags you need to know."/>
```
### Keywords

With keyword tags, website operators can define keywords for a search program. By entering information in this way, the search program will show the internet user HTML webpages with keywords corresponding to their search. HTML pages that contain the appropriate search terms in the keyword meta tag are given preference. The keyword tag used to be one of the most essential SEO meta tags, as primitive search engines used this as the central feature for search result rankings. However, because of the great potential to manipulate this meta element, it is now left out of Google’s ranking factors. Other search engine giants such as Bing also place less value on meta tag keywords, therefore the relevance of these meta tags for search engine optimization is close to zero. 

```html
<meta name="keywords" content="keyword 1, keyword 2, keyword 3"/>
```
### Author and Copyright

The “Author” and “Copyright” meta tags can be found in the source code of an HTML page. They are used to record information such as who built the corresponding website and who copyright ownership belongs to. The author tag is automatically inserted in some content management systems (CMS), and always gives the name of the last person who edited the page. The advantage of this is that the administrators in the source directory can ascertain who has edited which page. However, the use of these HTML meta tags is optional.

```html
<meta name="author" content="Author name" />

<meta name="copyright" content="Copyright owner" />
```

## References

1. https://www.javatpoint.com/css-box-model
2. https://www.geeksforgeeks.org/difference-between-block-elements-and-inline-elements/
3. https://leannezhang.medium.com/difference-between-css-position-absolute-versus-relative-35f064384c6
4. https://www.web4college.com/css/web4-css-structural-classes.php
5. https://www.geeksforgeeks.org/selectors-in-css/
6. https://css-tricks.com/specifics-on-css-specificity/
7. https://www.toptal.com/responsive-web/introduction-to-responsive-web-design-pseudo-elements-media-queries
8. https://www.freecodecamp.org/news/css-flexbox-tutorial-with-cheatsheet/
9. https://www.freecodecamp.org/news/css-grid-tutorial-with-cheatsheet/
10. https://www.ionos.com/digitalguide/websites/web-development/the-most-important-meta-tags-and-their-functions/