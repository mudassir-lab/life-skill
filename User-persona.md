# User Persona for Instagram
## User 1:

Type: Normal user.

Age: 15-50

Goals:

1. Connect with friends.
2. Follow celebrities 
3. Enjoy posts and videos for fun
5. Options to like and comment.

Frustrations
1. Don't want big videos.
2. Don't want repetitive posts

Environment

1. Mostly mobile phone(iOS or Android)

## User 2:

Type: Artist or Celebrity

Age: 10-70

Goals:

1. Show his art or post life updates.
2. Attract people.
3. Earn money by promoting products.


Frustrations
1. Don't want difficult processes to upload posts.
2. Need simple UI for statistics

Environment

1. Laptop or Tablet or Phone.

## User 3:

Type: Product seller

Age: 15-70

Goals:

1. Want to sell his products
2. Attract people
3. Earn money by promoting products


Frustrations
1. Don't want difficult processes to upload posts.
2. Need simple UI for statistics

Environment

1. Laptop or Tablet or Phone.



