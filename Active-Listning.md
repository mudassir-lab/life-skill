# Listening and Active Communication

## Active Listening

### What are the steps/strategies to do Active Listening? 

1. Avoid getting distracted by your own thoughts. Focus on the speaker and topic instead.
2. Try not to interrupt the other person. Let them finish and then respond.
3. Use door openers. These are phrases that show you are interested and keep the other person talking.
4. Show that you are listening with body language.
5. If appropriate take notes during important conversations.
6. Paraphrase what others have said to make sure you are on the same page.

## Reflective Listening

### According to Fisher's model, what are the key points of Reflective Listening?

1. Reflective listening involves two strategy

* Trying to understand a speaker's idea
* And offering idea back to speaker, to confirm the idea.

2. When reflecting on words of the person, keep their word and nonverbal clues in mind to create a reflection.

## Reflection 

### What are the obstacles in your listening process?

1. Interrupting another person
2. Getting distracted by own thoughts
3. Not showing proper body language

### What can you do to improve your listening?

1. By repeating what speaker has said
2. Trying to understand speaker idea
3. By taking notes 
4. paying full attention

## Types of Communications

### When do you switch to Passive communication style in your day to day life?

1. When we fear of being disliked
2. Getting made fun
3. Want to seem easygoing

### When do you switch into Aggressive communication styles in your day to day life?

1. When other person create pressure on me
2. When other person ignore me

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

1. When other person is superior
2. Or Stronger

### How can you make your communication assertive?

1. By expressing our feeling and respecting other person feeling
2. Start practising with safe people
3. By not screaming
4. keeping tone normal,
not too low and not too high